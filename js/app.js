// Brewtique by New Republique

$(document).ready(function(){
	// Check if there's an existing beer
	// (if shared to Facebook)
	// brewtiquebeer.com/#Word1#Word2#Word3
	var originalUrl = $(location).attr('href');
	var splitParts = originalUrl.split("#");
	var word1 = splitParts[1];
	var word2 = splitParts[2];
	var word3 = splitParts[3];

	// Set text on page to beer in url
	$('.name-one').text(word1)
	$('.name-two').text(word2)
	$('.name-three').text(word3)

	// Enable slab text 
	slabTextHeadlines()

  	// Generate random fonts
	var number = 1 + Math.floor(Math.random() * numberOfFonts);
	$('.labelText .name-one').alterClass('font-*', 'font-' + number);
	var number = 1 + Math.floor(Math.random() * numberOfFonts);
	$('.labelText .name-two').alterClass('font-*', 'font-' + number);
	var number = 1 + Math.floor(Math.random() * numberOfFonts);
	$('.labelText .name-three').alterClass('font-*', 'font-' + number);

	// Set icon to relevant .png
	$('.theIcon').attr('src', 'images/icons/' + $('.labelText .name-two').text() + '.png')
});

// When everything is loaded...
$(window).load(function(){
	
	// Enable slab text
	slabTextHeadlines()
	// Wait a bit, then do some stuff
	setTimeout(function() {
		// Hide the loading screen
		$('.loading').fadeOut(300);
		// Reset the URL (in case there was #Word1#Word2#Word3 in there)
		history.pushState("", document.title, window.location.pathname);
		// If the beer titles contain something (e.g. the user came from a shared post)
		if($('.titleContain .name-one').text().length > 0){
			// enable fb sharing
			$('.shareBev').show();
			// pick an intro phrase
			var number = 1 + Math.floor(Math.random() * numberOfStarters);
			$('.starter').text(starters[number])
			// slide the content up and reveal the beer title
			$('.header').animate({
				'margin-top': 0
				}, 500, function(){
				$('.titleContain').fadeIn(500);
			})
		}
	}, 700);
});

// Define available words as arrays because I am a terrible developer
var adjectives = new Array("Angry","Foaming","Rusty","Docile","Comatose","Flacid", "Erect", "Bonified", "Mexican", "Spanish", "Hardened", "Cheeky", "Crusty", "Moist", "Hipster", "Bavarian", "Moon", "Ye Olde", "Big", "Tiny", "Triumphant", "Steam-powered", "Old British", "Horny", "Curious", "Happy", "Sleepy", "Furious", "Bouncing", "Dripping", "Slimey", "Floating", "High as a Kite", "Criminal", "Premium", "Stone Cold",  "Riverside", "Slightly Crooked", "Corrupt", "A Big Jug of Bitter", "Earnest", "Leather", "Plastic", "Rock-hard", "Steaming", "Choking", "Pint of Naughty", "Nostalgic", "Yo Mama's", "Rare Endangered", "Rotten", "Smokey", "Sensual", "Shaken Not Stirred", "Crafty", "Bottled", "Magic", "Hand-crafted","Frozen", "Ice-cold", "Sharp", "Papa's Own", "Australia's Finest", "Gagging", "Saggy", "Giggly", "Flaming", "Slippery", "Inappropriate", "Creepy Uncle's", "Slappy", "Awkward", "Busty", "Busty", "Lychee-infused", "Violent", "Nima's", "Brabz'", "Becky's", "Sofia's", "AJ's", "Jackson's", "Rob's", "Yvette's", "Lucy's", "Sophie's", "Diane's");

var nouns = new Array("Goat", "Hog", "Mountain", "Bear", "Beard", "Bottle", "Trombone", "Tulip", "Strawberry", "Melon", "Poop", "Bearded Man", "Astronaut", "Floater", "Moon", "Cow", "Shart", "Man", "Goblin", "King", "David Bowie", "Wolf", "Cat", "Dog", "Eagle", "Salmon", "Steve Austin", "Liam Neeson", "Hemmingway", "Stiffy", "Salad",  "Panda", "Schoolbus", "Meat", "Fruit", "Plant", "Tree", "Breeze", "Smoke", "Liquid", "Pint Glass", "Spoon", "Stinkhole", "Yeti", "Nomad", "Mermaid", "Lobster", "Nude Man", "Sword", "Cutlass", "Dagger", "Pirate", "Pirate's Chest", "Booty", "Panther", "Gypsy", "Flipper", "Dolphin", "Tiger", "Sasquatch", "Cthulhu", "Lumberjack", "Terrier", "Minotaur", "Leprechaun", "Centaur", "Hydra", "Gary Oldman", "Diplodocus", "Dinosaur", "Lizard Queen", "French Bulldog", "Portuguese Man", "Skidmark", "Poo Stain", "Gnome", "Llama", "Burnt Dog", "Drop Bear", "Whirlybird", "Prohibition", "Speakeasy", "Downstairs" );

var types = new Array("Ale", "Pilsner", "Lager", "Draught", "Beer", "Dry", "Dark Ale", "Sunrise", "Whisky", "Surprise", "Delight", "On the Beach", "Bitter", "Hightail", "Pale Ale", "Original", "Hops", "Of Darkness", "Cider", "Served in a Man's Boot", 'Golden Ale', "Transfusion", "Robot", "Infusion", "Inside a Box of Crayons", "Floating in an Ashtray", "Scrote", "Tribute", "Scotch Whisky", "Explosion", "Spritz", "Mixer", "Half-strength Ale", "Yardglass", "Premium", "Classic", "Premium Ale", "Classic Lager", "Full-strength", "Craft", "Amber Ale", "Pacific Ale", "Natural Cider", "Sparkle", "Ginger Beer", "Steam Beer", "Wheat Beer", "Light", "Lime & Bitters", "& Lime", "Served by an Otter", "Shot out of a Cannon", "Sour Ale", "Frosted Scotch"); 

var starters = new Array("Maybe you'd like a...", "I'd suggest a bottle of...", "The beer of the day is...", "Hipsters can't get enough of...", "Your mother always orders...", "Most bearded men prefer...", "You've got to try...", "It's cold. It's imported. It's...", "The locals always order...", "All the beautiful people drink...", "All the cool kids drink...", "Could we suggest...", "This week's special is...", "There's always...", "All my fixie-riding friends swear by...", "Real beer drinkers love...", "Aficionandos always choose...", "Beer buffs love...", "Try a...", "Fancy a...", "How about a...", "You look like you could do with a...", "Take your shirt off and order some...", "A New Republique favorite...");

// Grab number of words in each category
var numberOfAdjectives = adjectives.length
var numberOfNouns = nouns.length
var numberOfTypes = types.length
var numberOfStarters = starters.length

// Set to number of fonts in each 'bank' of fonts
var numberOfFonts = 5

// Get new words when button is clicked
$('.generate').click(function(e){	
		e.preventDefault();
		// slide the header up (if it isn't already), and reveal the title 
		$('.header').animate({
			'margin-top': 0
		}, 500, function(){
			$('.titleContain').fadeIn(500);
		})

		// Set the three word types (adjective, noun, type)
		var number = 1 + Math.floor(Math.random() * numberOfAdjectives);
		$('.name-one').text(adjectives[number])
		currentAdjective = $('.titleContain .name-one').text();
		
		var number = 1 + Math.floor(Math.random() * numberOfNouns);
		$('.name-two').text(nouns[number])
		currentNoun = $('.titleContain .name-two').text();

		var number = 1 + Math.floor(Math.random() * numberOfTypes);
		$('.name-three').text(types[number])
		currentType = $('.titleContain .name-three').text();

		// Set the icon to the current noun
		$('.theIcon').attr('src', 'images/icons/' + currentNoun + '.png')

		// Pick random fonts
		var number = 1 + Math.floor(Math.random() * numberOfFonts);
		$('.labelText .name-one').alterClass('font-*', 'font-' + number);
		var number = 1 + Math.floor(Math.random() * numberOfFonts);
		$('.labelText .name-two').alterClass('font-*', 'font-' + number);
		var number = 1 + Math.floor(Math.random() * numberOfFonts);
		$('.labelText .name-three').alterClass('font-*', 'font-' + number);

		// Pick a random intro sentence
		var number = 1 + Math.floor(Math.random() * numberOfStarters);
		$('.starter').text(starters[number])

    	// enable slab text
    	slabTextHeadlines()

    	// show the fb share button
    	$('.shareBev').show();

    	// fade in mobile share if on mobile
    	$('.mobileFB').fadeIn(300);
}); 

// Facebook SDK
$('.shareBev').click(function(){
	FB.ui(
      {
       method: 'feed',
       name: currentAdjective + ' ' + currentNoun + ' ' + currentType + ' ' + 'on' + ' ' + 'Brewtique',
       caption: "Brewtique by New Republique",
       description: (
          'I just brewed' + ' ' + currentAdjective + ' ' + currentNoun + ' ' + currentType + ' using Brewtique: the damn sexy Hipster beer generator. Brew your own today!'
       ),
       link: 'http://brewtiquebeer.com/#' + currentAdjective + '#' + currentNoun + '#' + currentType,
       picture: 'http://newrepublique.com/brewtique.png' 
      },
      function(response) {
        if (response && response.post_id) {
          $('.success').fadeIn(300)
        } else {
        }
      }
    );
});

// lightbox for icon credits
$('.openCreds').click(function(){
	$('.creds').fadeIn(300)
});
$('.creds').click(function(){
	$(this).fadeOut(300);
});

// success message on fb share
$('.success .close').click(function(){
	$(this).parent().fadeOut(300)
});




// Obligatory BoC reference 
console.log('You could feel the sky.');



